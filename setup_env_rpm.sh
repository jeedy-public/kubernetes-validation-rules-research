#!bin/bash
set -x
# install dependencies: git, make, go, kubectl, etcd.
sudo yum update -y
sudo yum install git make -y
## more generic than | .deb and .rpm
sudo yum install golang-1.20.10-1.el9_3 -y
sudo yum install kubectl-1.28.1-0 -y
sudo yum install delve-1.20.2-1.el9 -y
# TODO, missing kubectl version

# install kubectl
#cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
#[kubernetes]
#name=Kubernetes
#baseurl=https://pkgs.k8s.io/core:/stable:/v1.28/rpm/
#enabled=1
#gpgcheck=1
#gpgkey=https://pkgs.k8s.io/core:/stable:/v1.28/rpm/repodata/repomd.xml.key
#EOF
#sudo yum install kubectl -y
# alternatively you may clone etcd from and build from sources, usage as system service is not necessary: https://etcd.io/docs/v3.5/install/

## etcd installation
#yum install epel-release -y
#yum install snapd -y
#systemctl enable --now snapd.socket
#ln -s /var/lib/snapd/snap /snap
#snap install --channel=3.4/stable etcd
## etcd installation
#yum install etcd -y

#etcd installation 2nd way
yum install curl wget -y
# ETCD_RELEASE=$(curl -s https://api.github.com/repos/etcd-io/etcd/releases/latest|grep tag_name | cut -d '"' -f 4)
ETCD_RELEASE=v3.4.28
echo $ETCD_RELEASE
wget https://github.com/etcd-io/etcd/releases/download/${ETCD_RELEASE}/etcd-${ETCD_RELEASE}-linux-amd64.tar.gz
tar xvf etcd-${ETCD_RELEASE}-linux-amd64.tar.gz
cd etcd-${ETCD_RELEASE}-linux-amd64
sudo mv etcd* /usr/local/bin
ls /usr/local/bin
cd ..
#cp etcd-${ETCD_RELEASE}-linux-amd64/etcd .
<etcd --version
#ensure you have it as system deamon or running it in terminal during debugging kube-apiserver

# clone kube-apiserver (consider direct-branch-checkout instead of clone)
git clone -b release-1.27 --single-branch https://github.com/kubernetes/kubernetes
# create certs needed to run

## service account
openssl genrsa -out service-account-key.pem 4096 && openssl req -new -x509 -days 365 -key service-account-key.pem -subj "/CN=test" -sha256 -out service-account.pem

## api-server
openssl genrsa -out ca.key 2048 && openssl req -x509 -new -nodes -key ca.key -subj "/CN=test" -days 10000 -out ca.crt && openssl genrsa -out server.key 2048 && openssl req -new -key server.key -out server.csr -config csr.conf &&
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key \
    -CAcreateserial -out server.crt -days 10000 \
    -extensions v3_ext -extfile csr.conf

echo "END: Well done! Etcd installed, kubernetes repository cloned, certificates created, all necessary dependencies are here"
