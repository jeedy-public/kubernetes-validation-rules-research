# 0. Prerequisites
Any environment with installed: `git`, `make`, `go`, `etcd`, `kubectl`.

Prepared certs - you can use also prepared .conf included in the repository: `csr.conf`.
In our example it will be Ubuntu 22.04 (same steps possible in Debian):

```
# install dependencies: git, make, go, kubectl, etcd.
apt-get update
apt-get install git make -y
snap install go --channel=1.21/stable --classic
snap install kubectl --classic
# alternatively you may clone etcd from and build from sources, usage as system service is not necessary: https://etcd.io/docs/v3.5/install/
apt-get install etcd -y

# clone kube-apiserver (consider direct-branch-checkout instead of clone)
git clone https://github.com/kubernetes/kubernetes
cd kubernetes
git checkout release-1.27

# create certs needed to run

## service account
openssl genrsa -out service-account-key.pem 4096 && openssl req -new -x509 -days 365 -key service-account-key.pem -subj "/CN=test" -sha256 -out service-account.pem

## api-server
openssl genrsa -out ca.key 2048 && openssl req -x509 -new -nodes -key ca.key -subj "/CN=test" -days 10000 -out ca.crt && openssl genrsa -out server.key 2048 && openssl req -new -key server.key -out server.csr -config csr.conf &&
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key \
    -CAcreateserial -out server.crt -days 10000 \
    -extensions v3_ext -extfile csr.conf
```

# 1. Fast environement setup and certificates

It will: install dependencies: git, make, go, kubectl, etcd; clone kube-apiserver; setup certs for service account and api-server. 

## For Ubuntu (deb):
Run `bash setup_env_deb.sh`

## For AlmaLinux (rpm):
Run `bash setup_env_rpm.sh`

## #1.1. Build kube-apiserver
(as part of kubernetes project)

Open terminal inside directory and run:
`make`

`git clone https://github.com/kubernetes/kubernetes && cd kubernetes`
`git checkout release-1.27`

## Check if binary works.
Expected output like:
`Kubernetes v1.27.8-3+f76ee87da1dbab`

Okay, the binary works.

# 2. Setup kubeconfig
In terminal:
```
# kubeconfig
kubectl config set-cluster local-apiserver \
--certificate-authority=ca.crt \
--embed-certs=true \
--server=https://127.0.0.1:6443 \
--kubeconfig=kubeconfig

kubectl config set-credentials admin \
--client-certificate=server.crt \
--client-key=server.key \
--embed-certs=true \
--kubeconfig=kubeconfig

kubectl config set-context default \
--cluster=local-apiserver \
--user=admin \
--kubeconfig=kubeconfig

kubectl config use-context default --kubeconfig=kubeconfig
```
# 3. Let's run!
```
 ./_output/bin/kube-apiserver --etcd-servers http://localhost:2379 \
--service-account-key-file=service-account-key.pem \
--service-account-signing-key-file=service-account-key.pem \
--service-account-issuer=api \
--tls-cert-file=server.crt \
--tls-private-key-file=server.key \
--client-ca-file=ca.crt
```
Hint: if you don't use `etcd` as system deamon, you may run this in the background, e.g.:
`nohup etcd &` or `nohup timeout 100 etcd &` just for testing purposes.

Expected kind of output from `etcd`: 
```
{"level":"info","ts":"2023-11-21T16:09:51.875342+0100","caller":"embed/etcd.go:378","msg":"closed etcd server","name":"default","data-dir":"default.etcd","advertise-peer-urls":["http://localhost:2380"],"advertise-client-urls":["http://localhost:2379"]}
```
Expected kind of output from kube-apiserver (when etcd set up correctly):
```
I1121 16:10:54.663394   72847 dynamic_cafile_content.go:171] "Shutting down controller" name="client-ca-bundle::ca.crt"
I1121 16:10:54.663441   72847 controller.go:159] Shutting down quota evaluator
I1121 16:10:54.663460   72847 controller.go:178] quota evaluator worker shutdown
I1121 16:10:54.663939   72847 secure_serving.go:258] Stopped listening on [::]:6443
I1121 16:10:54.663965   72847 tlsconfig.go:255] "Shutting down DynamicServingCertificateController"
I1121 16:10:54.664157   72847 dynamic_serving_content.go:146] "Shutting down controller" name="serving-cert::server.crt::server.key"
```

# 4. Debug / dockerization / using GoLand / VisualCode

++ add info for debugging
++ for different systems

dziala to w miare na ubuntu I guess
jeszcze dockerki musze przygotowac, jakby ktos se chcial postawic etcd / srodowisko do budowania w dockerze
a czy apiserver w dockerze ruszy, to nie wiem, zobaczymy
moze nawet docker compose... chociaz ten apiserver to moze lepiej poza dockerem do debugowania
lub rzuce okiem jak zrobic remote debug (robilem to kiedys w swiecie javy, mam nadzieje, ze w swiecie go bedzie podobnie) :D


5. GoLand env preparation


6. VisualCode env preparation



7. Docker compose



8. Ingress example of gathering rules in various places

9. Some article about this

10. Current rules example against hidden-in-code validation rules

11. If there will be enough time: let's check news from 1.27 OpenAPIv3.

+ sample api server project to check and past notes

readme
testing for debian/ubuntu and rpm/almalinux/centos
setup testing
+ how to debug in goland
+ plus focus on other things, I would like to finish research plan/repeating steps and presenting for any researcher what we archiwed, what hourt us, how to repeat the steps, continue the research etc.
+ and i still wanted to look at this openapiv3. i found some notes in my notes from few months when we were discussing about schemas for 1.27, however! i didnt spot any tool with support for v3 in golang world.
+ checking other languages, hope it will be anywhere else not only in js.

https://kubernetes.io/blog/2023/04/24/openapi-v3-field-validation-ga/

https://kubernetes.io/docs/reference/using-api/cel/

https://github.com/kubernetes/enhancements/issues/2885

These two features are driven by the SIG API Machinery community, available on the slack channel #sig-api-machinery, through the mailing list and we meet every other Wednesday at 11:00 AM PT on Zoom.


22_11_2023:

added support for debian based distros and alma based, from my testing in our vms both works fine
readme + one-liner scripts how to setup everything that is needed to repeat our steps
waiting for antonio's checking

writing debuging configuration + screenshots how to configure and run debugger in GoLand community edition
+ visual code if enough time

after that, put our testing scenario and hope readme will be also project and obstacles description

i want to finish it as soon as possible to get some time to investigate 
openapi v3 server side validaiton that was recently introduced

TODO
  remote debugging - check; check debugger against standalone binary somewhere this kube-apiserver standalone binary
  remote debugging against binary in docker... and/or via ssh
  remote debugging against real apiserver in kubernetes (if possible, because they probably erased this? maybe let's check it in minikube)
  ogarnij flage do debugowania i zbuduj binarke i se ja zdalnie ogarnij, i potem zdalnie przez wyeksponowanego dockera, 
  i zdalnie przez ssh (jesli starczy czas)
  + docker compose :D byłoby spoko, taki full scenariusz, edytujesz kod i wszystko wstaje jak trzeba
  + albo samo etcd
  + albo etcd + owrapowane zbudowane kube-apiserver
// 22_11_2023

+ goland community edition run debug put breakpoint here and check
+ maybe similar with visual code

we can cofnrim it works well and mostly authomatically in deb and rpm linuxes, great.
debugging using goland community edition also works well!
with visual code also <- finishing

dockerization - ongoing...

you need this this this

this is how to archieve it in this kind of env and this

there's also one run script that should do most of these things for you.


UPDATE standup dwa rozne system wsparcie buduje sie etcd bangla itd.
debugowanie ongoing na goland i visual cude z obrazkami itd.
goarniam jeszcze remote, zeby sie wyrobic ze wszystkim + repeat steps jakie zrobic, zeby zobaczyc tego ingressa
staram sie zeby wszystko dopiac mozliwie szybko, zeby starczylo czasu na rozejrzenie sie za openapi v3... i to serwer side validaiton co oni tam wykminili jak w linkach
future resereach: this is something new, started as general availability from 1.27... very new comparing to the time when we started to work on our project

+ compiler options as for:
+ go build -gcflags="all=-N -l" -o myApp
  Go 1.9 and earlier:

go build -gcflags="-N -l" -o myApp
This command compiles the myApp executable and disables compiler optimizations and inlining.

hints how to get community edition goland visual code etc.


dlv = Delve
If you wanna build using GoLand you may skip these steps, cuz dlv is pre-installed/pre-built into this IDE.

# Debug stuff

## If you wanna use other tools and take care of your own dlv, please use these steps:
DWARF + Delve Expanation
### For Debian:
apt-get -y install delve

Delve is used automatically by Go from 1.10 albo 1.11 and Newer



15. WE NEED to build binary with falgs
16. to make debugger DWARF works and DWARF some changes



Example z ingressem:
kubectl apply -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml
kubectl delete -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml



Questions:
how to compare codebase and remote debugger? that is somewhere like in docker or cluster



/home/adrian/Documents/GoLand-2023.2/plugins/go-plugin/lib/dlv/linux/dlv --listen=127.0.0.1:37433 --headless=true --api-version=2 --check-go-version=false --only-same-user=false exec /home/adrian/.cache/JetBrains/GoLand2023.2/tmp/GoLand/___go_build_k8s_io_kubernetes_cmd_kube_apiserver -- --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt
API server listening at: 127.0.0.1:37433




zakupy:
pojemnik na klucze do pływania / Saszetka wodoszczelna Decathlon
kąpielówki z zamkiem



kubectl apply -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml --kubeconfig kubeconfig
kubectl delete -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml --kubeconfig kubeconfig

## To get PID by process name, e.g.
pgrep kube-apiserver

potem: e.g.
dlv --listen=:2345 --headless=true --api-version=2 attach 670609
za port wstaw prawdziwy port

Remote debug w GoLand (port! kolorkiem)

Remote debug w VSCode

update vscode robimy
uzasadniamy, ze dziala tu i tu i nara

i ew. nagraj te terminale i pokaz, ze dziala
i zamien film na gifa albo cos innego, co malo wazy :D tez bedzie fajnie wygladac

kubectl get componentstatuses --kubeconfig kubeconfig

d oraportu tego delve, moze pwndbg, zdjecia pododawac, filmiki jakie to fajne, ile tam rzeczy do sprawdzenia, ile calli to generuje
jakos zdumpowac liste funkcji callowanych :D

kubectl run nginx --image nginx --kubeconfig kubeconfig

kubectl delete pod nginx --kubeconfig kubeconfig


19_11_2023


zdeployuj te dwie rzeczy i zobacz co wypluje kosnola/debugger
albo stworz apiserver z takim innym deubgerem
i sprobuj ten apiserver spiac z codesource, mimo ze jest on gdzies tam daleko dalej
jak spiac binarke "gdzies tam" z kodem "ktory jest u mnie"



daj info jak develowac jakakolwiek binarke
w tym taka, co mamy gdzies tam
according to tego co jest w goland
in goland we have this for free
in visualcode, let's take a moment!
for golang 1.11 >= jadom, mamy default dvelwe albo niedefault
robimy cos i bangla odpalamy i jest git :D widzi nasza binarke tu i tam via docker ssh remote debugging
+ podpiecie codebase i jest gituwa.


#   DBG: If set to "1", build with optimizations disabled for easier
#     debugging.  Any other value is ignored.

## Delve (dlv)

go install github.com/go-delve/delve/cmd/dlv@latest




date && make kube-apiserver -d DBG=1 && date



Log verbosity descriptions
Log verbosity	Description
--v=0

Always visible to an Operator.

--v=1

A reasonable default log level if you do not want verbosity.

--v=2

Useful steady state information about the service and important log messages that might correlate to significant changes in the system. This is the recommended default log level.

--v=3

Extended information about changes.

--v=4

Debug level verbosity.

--v=6

Display requested resources.

--v=7

Display HTTP request headers.

--v=8

Display HTTP request contents.




./kube-apiserver --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt --v=4




kubectl apply -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml --kubeconfig kubeconfig
ingress.networking.k8s.io/minimal-ingress created
kubectl delete -f minimal-ingress-adrian-test-example-port-is-not-mandatory.yaml --kubeconfig kubeconfig
ingress.networking.k8s.io "minimal-ingress" deleted




dlv --listen=:2379 --headless=true --api-version=2 --accept-multiclient exec ./demo
dlv --listen=:6443 --headless=true --api-version=2 --accept-multiclient exec ./demo


dlv --listen=:2379 --headless=true --api-version=2 --accept-multiclient exec ./kube-apiserver2


dlv --listen=:2379 --headless=true --api-version=2 --accept-multiclient exec ./kube-apiserver2 --etcd-servers http://localhost:2379 \
--service-account-key-file=service-account-key.pem \
--service-account-signing-key-file=service-account-key.pem \
--service-account-issuer=api \
--tls-cert-file=server.crt \
--tls-private-key-file=server.key \
--client-ca-file=ca.crt




/home/adrian/Documents/GoLand-2023.2/plugins/go-plugin/lib/dlv/linux/dlv --listen=127.0.0.1:36221 --headless=true --api-version=2 --check-go-version=false --only-same-user=false exec /home/adrian/.cache/JetBrains/GoLand2023.2/tmp/GoLand/___go_build_k8s_io_kubernetes_cmd_kube_apiserver -- --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt


REMOTE DEBUGGING! mordo.
zapnij sie do binarki, ktora odpaliles lokalnie i jakos z breakpointem i w ogole...
:( w goland zrobmy wszystko chociaz, potem jakis terminale i duperele)


You can connect to a remote computer (a host) and attach the debugger to the Go process that runs on the host. The remote debugger (Delve) must be running on the remote computer.

that's why it was tricky...

i will let you know how to easly debug binary compiled in goland using delve built-in goland, but there are other cases.

Could not attach to pid 320441: this could be caused by a kernel security setting, try writing "0" to /proc/sys/kernel/yama/ptrace_scope

ja pierdolę, właśnie zmieniam jakieś rzeczy w kernelu, żeby mi debugger w `go` działał... tęsknię do javy
https://www.kernel.org/doc/html/v4.15/admin-guide/LSM/Yama.html


Yama
Yama is a Linux Security Module that collects system-wide DAC security protections that are not handled by the core kernel itself. This is selectable at build-time with CONFIG_SECURITY_YAMA, and can be controlled at run-time through sysctls in /proc/sys/kernel/yama:

ptrace_scope



Archivements:
* DWARF
Package dwarf provides access to DWARF debugging information loaded from executable files, as defined in the DWARF 2.0 Standard at http://dwarfstd.org/doc/dwarf-2.0.0.pdf.
* Delve
Delve is a debugger for the Go programming language. The goal of the project is to provide a simple, full featured debugging tool for Go. Delve should be easy to invoke and easy to use. Chances are if you're using a debugger, things aren't going your way. With that in mind, Delve should stay out of your way as much as possible.

* Golang debuger, however it must run as a server, so
you must have it enabled next to your application - there are two ways, either you inject it by wrapping the application or you connect it to the port

how to build with dwarf notes and only subset of kubernetes resources - also done

to enable pid-style debug I had to read a little about kernel yama
https://www.kernel.org/doc/html/v4.15/admin-guide/LSM/Yama.html
and ptrace_scope

tested everything with silly app - debugger works and also breakpoint putted in the code (even if application is somewhere else) worked

--v=4
--v=8 requesty mozna podejrzec :D
https://docs.openshift.com/container-platform/4.8/rest_api/editing-kubelet-log-level-verbosity.html

https://kubernetes.io/docs/concepts/cluster-administration/logging/

https://github.com/kubernetes/enhancements/issues/2885



https://github.com/google/gops/
gops is a command to list and diagnose Go processes currently running on your system.


