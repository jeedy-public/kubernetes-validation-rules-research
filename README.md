# 0. Prerequisites
Any environment with installed: `git`, `make`, `go`, `etcd`, `kubectl`, `sudo`.

For later, there would be necessary to install also `delve`.

**Prepared certs** 

For convenience, you can use prepared .conf included in the repository: `csr.conf`.

However, I advise you to create your own certificate (that will be needed by kube-apiserver) as described in below.
In our example it will be Ubuntu 22.04 (same steps possible in Debian):
#### Dependencies:
```
# INSTALL DEPENDENCIES: git, make, go, kubectl, etcd.
sudo apt-get update
sudo apt-get install git make -y
snap install go --channel=1.21/stable --classic
snap install kubectl --classic
# alternatively you may clone etcd from and build from sources, usage as system service is not necessary: https://etcd.io/docs/v3.5/install/
apt-get install etcd -y

# clone repository with kube-apiserver
git clone -b release-1.27 --single-branch https://github.com/kubernetes/kubernetes
```
#### Certificates part:
```
# CREATE CERTIFICATES needed to run
## service account
openssl genrsa -out service-account-key.pem 4096 && openssl req -new -x509 -days 365 -key service-account-key.pem -subj "/CN=test" -sha256 -out service-account.pem

## api-server
openssl genrsa -out ca.key 2048 && openssl req -x509 -new -nodes -key ca.key -subj "/CN=test" -days 10000 -out ca.crt && openssl genrsa -out server.key 2048 && openssl req -new -key server.key -out server.csr -config csr.conf &&
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key \
    -CAcreateserial -out server.crt -days 10000 \
    -extensions v3_ext -extfile csr.conf
```

# 1. Fast environment setup and certificates

Pre-automated scripts that will: install dependencies: `git`, `make`, `go`, `kubectl`, `etcd` and clone kube-apiserver, and setup certs for service account and api-server. 

## 1.1. For Debian/Ubuntu (deb):
Run `bash setup_env_deb.sh`.

## 1.2. For AlmaLinux/CentOS (rpm):
Run `bash setup_env_rpm.sh`.

## 1.3. Build kube-apiserver
(as part of kubernetes project)

`kube-apiserver` is entry point for all creating resources calls.

### 1.3.1 Clone repository in case you didn't use scripts above:
`git clone -b release-1.27 --single-branch https://github.com/kubernetes/kubernetes`

### 1.3.2 Open terminal inside kubernetes directory and run:
`date && make kube-apiserver -d DBG=1 && date`

Note about DBG: If set to `1`, build with optimizations disabled for easier debugging. `DBG=1` is crucial to keep `DWARF` data needed by Delve debugger, otherwise we won't be able to use breakpoints in remote debug.
`make` will build module `kube-apiserver` from kubernetes' `Makefile`.

### 1.3.3 Check if binary works.
Expected output like:
`Kubernetes v1.27.8-3+f76ee87da1dbab`

Okay, the binary works.

# 2. Setup kubeconfig
`kubeconfig` is needed to make easy communication to `kube-apiserver` using `kubectl`.

In terminal:
```
# kubeconfig
kubectl config set-cluster local-apiserver \
--certificate-authority=ca.crt \
--embed-certs=true \
--server=https://127.0.0.1:6443 \
--kubeconfig=kubeconfig

kubectl config set-credentials admin \
--client-certificate=server.crt \
--client-key=server.key \
--embed-certs=true \
--kubeconfig=kubeconfig

kubectl config set-context default \
--cluster=local-apiserver \
--user=admin \
--kubeconfig=kubeconfig

kubectl config use-context default --kubeconfig=kubeconfig
```
# 3. Let's run!
Section explaining how to run everything.
## 3.1 etcd - key-value database for kube-apiserver 
Please ensure you have working `etcd`. It should work as system daemon if you installed it correctly as described in above sections or at least be available from shell session, so you can run it using e.g. 2nd terminal.

Hint: if you don't use `etcd` as system daemon, you may run this in the background, e.g.:
`nohup etcd &` or `nohup timeout 100 etcd &` just for testing purposes, or just keep `etcd` in separated shell session.

#### Expected kind of output from `etcd`:
```
{"level":"info","ts":"2023-11-21T16:09:51.875342+0100","caller":"embed/etcd.go:378","msg":"closed etcd server","name":"default","data-dir":"default.etcd","advertise-peer-urls":["http://localhost:2380"],"advertise-client-urls":["http://localhost:2379"]}
```
## 3.2 kube-apiserver
In terminal:
```
 ./_output/bin/kube-apiserver --etcd-servers http://localhost:2379 \
--service-account-key-file=service-account-key.pem \
--service-account-signing-key-file=service-account-key.pem \
--service-account-issuer=api \
--tls-cert-file=server.crt \
--tls-private-key-file=server.key \
--client-ca-file=ca.crt
```

#### Expected kind of output from kube-apiserver (when etcd set up correctly):
```
I1121 16:10:54.663394   72847 dynamic_cafile_content.go:171] "Shutting down controller" name="client-ca-bundle::ca.crt"
I1121 16:10:54.663441   72847 controller.go:159] Shutting down quota evaluator
I1121 16:10:54.663460   72847 controller.go:178] quota evaluator worker shutdown
I1121 16:10:54.663939   72847 secure_serving.go:258] Stopped listening on [::]:6443
I1121 16:10:54.663965   72847 tlsconfig.go:255] "Shutting down DynamicServingCertificateController"
I1121 16:10:54.664157   72847 dynamic_serving_content.go:146] "Shutting down controller" name="serving-cert::server.crt::server.key"
```

Now we can confirm that `kube-apiserver` works good with `etcd` and previously prepared certificates. Great job!

# 4. Debug / dockerization / using GoLand / VisualCode
Debugging is the process of identifying and removing errors from computer hardware or software.

Explanation of used tools and words:
* DWARF

  Package dwarf provides access to DWARF debugging information loaded from executable files, as defined in the DWARF 2.0 Standard at http://dwarfstd.org/doc/dwarf-2.0.0.pdf.
* Delve

  Delve is a debugger for the Go programming language. The goal of the project is to provide a simple, full-featured debugging tool for Go. Delve should be easy to invoke and easy to use. Chances are if you're using a debugger, things aren't going your way. With that in mind, Delve should stay out of your way as much as possible.
* Debugger 

  Debugger assists in the detection and correction of errors in other computer programs, in our case it must run as a server.

#### If you want to build (build, not debug existed somewhere `kube-apiserver` binary) using GoLand you may skip these steps, since dlv is pre-installed/pre-built into this IDE.

Using Delve all you need is to expose the sever port and inject dlv to app's PID (or start app wrapped by dlv itself), so technically you may remote debugging using any things like docker, over the Internet, etc.

## 4.1 Debugging in GoLand from running codebase
Support for dlv will be partially done by GoLand.

GoLand's installation guide:
https://www.jetbrains.com/help/go/installation-guide.html

Please:
* Start GoLand, open previously cloned project with `kubernetes` and create new configuration as below
![Create configuration in GoLand](img/goland_create_configuration.png)
* Build kube-apiserver and run this or debug it (**remember:** you have to have working `etcd` in the background - in shell or as system daemon):
![Build kube-apiserver](img/kube_apiserver_local_codebase.png)
Program arguments should be:
  ```
  --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt 
  ```
* Go to the package and file:
```kubernetes/pkg/apis/networking/validation/validation.go```

  ![Goland - project view validation](img/goland_project_view_validation.png)
* Put a breakpoint in the code, in `validation.go` for func `validateIngressBackend` in line no. 469.
  ![Goland - validate ingressbackend](img/goland_validate_ingressbackend.png)
  Note: please ensure you use debug (bug icon, not arrow).
* Send ingress using `kubectl`, in terminal:

  ```kubectl apply -f minimal-ingress-test-example-only-name-is-required-port-is-not-required-ingress-service-backend.yaml --kubeconfig kubeconfig```
* Check in debugger
  ![Goland - working debugger](img/goland_breakpoint_works_remote_debug.png)

  The icon should switch from red circle to red circle with checkmark.
  Note: you can see, a resource sent in request is fine (according to json schema described using OpenAPIv2):
   ![ingress schema valid visualisation](img/ingress_schema_valid_visusalisation.png)

but `kube-apiserver` will fail in the line **503** (`kubernetes -> pkg -> apis -> networking -> validation -> validation.go`, because of additional requirements put in the code (even if the object `backend.service.name` (in context of `IngressServiceBackend`) is not mandatory in json schema, it will fail due to additional, _hidden_ validation rules in the code)

Example of the problem:
The validation observed is not part of the public, provided OpenAPI schema. It is sewn into the codebase, which makes the code incosistent with the schema (according to the schema, the field is not required) and which leads to a multitude of possible inconsistencies with the code, which the user cannot find out using the schema, so the user is forced to either scan the whole code - an impossible task in any reasonable time, or try to deploy the resource to real cluster (using server-side `kube-apiserver`) to check if it is correct or not.

![validation_503_port_name.png](img%2Fvalidation_503_port_name.png)

**The clue of the problem (one of enormous set of examples to reproduce) is OpenAPI schema is valid, BUT the resource is wrong due to internal, hidden validation rules - example from the code that you can repeat is below.**

The OpenAPI doesn't say backend.Service.Port is required, only `name` is required in the schema (as in the code snippet below). In the code we can regonise that `port` is also required. It is not this is not easy to scrape from code and should just be part of the openapi schema, which would make life easier for all of us - all kubernetes users.
```
  for _, msg := range apivalidation.ValidateServiceName(backend.Service.Name, false) {
  allErrs = append(allErrs, field.Invalid(fldPath.Child("service", "name"), backend.Service.Name, msg))
  }
```
* you may also test with well described resource (regarding to the OpenAPI schema and also to kubernetes internal validating rules): `minimal-ingress-test-example-valid-ingress.yaml`
  ```
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
  name: minimal-ingress
  annotations:
  nginx.ingress.kubernetes.io/rewrite-target: /
  spec:
  ingressClassName: nginx-example
  rules:
  - http:
    paths:
    - path: /testpath
      pathType: Prefix
      backend:
      service:
        name: test2
      port:
       number: 80

  ```
* expected: created, no issues
* you may also remove the resource using kubectl delete

#### Note: if you stay too long in the debug session, the requrest may be killed during timeout! Try again if needed.

<a href="#example-of-ingressjson">Ingress schema to compare</a>

Available also as external resource here: https://gitlab.cern.ch/jeedy-public/kubernetes-json-schemas/-/raw/master/v1.27.1-standalone/ingress.json

## 4.2 Remote debugging in GoLand
Any kube-apiserver binary is welcome! 
Beware: if you erased DWARF data, breakpoints won't rune, because dlv could not associate code with real running functions.

## 5. GoLand env preparation
`dlv --listen=:2345 --headless=true --api-version=2 attach 670609`

`./kube-apiserver`

# Debug instructions

## If you would like to use other tools and take care of your own dlv, please use these steps:
### For Debian:

Delve is used automatically by Go from 1.10 albo 1.11 and Newer

**We need to** build binary with additional flags to ensure we won't erase metadata needed for debugger.

## To get PID by process name.
### Firstly, e.g.: 
`pgrep kube-apiserver`

### After this, 2: e.g.
Command: `dlv --listen=:2345 --headless=true --api-version=2 attach GREPPED_KUBE_APISERVER_PORT_HERE`

Example: `dlv --listen=:2345 --headless=true --api-version=2 attach 670609`

### Debugging using VSCode:
Instead of using GoLand, you may use open source alternative called VSCode.
* https://github.com/golang/vscode-go/wiki/debugging#remote-debugging
* https://stackoverflow.com/questions/59652312/how-to-remotely-debug-go-code-with-vscode
You need to load kubernetes codebase using VSCode and follow instructions from the 1st link that will allow you to set a breakpoint and associate it with selected lines of code from the loaded codebase.

In case of problems with DWARF, there's: `remotePath` element in `launch.json`, you may need to put absolute path to the source files as copiled on your local system (not the container).
* https://stackoverflow.com/questions/69936099/debugging-go-in-vscode-doesnt-stop-at-breakpoints-says-could-not-find-file
* https://www.google.com/search?q=vscode+debug+breakpoint+could+not+find+file&oq=vscode+debug+breakpoint+could+not+find+file

However, the tutorial will mostly focus on GoLand as the IDE most familiar to us. 

## Delve (dlv)
`Delve` is a debugger for the `Go` programming language.
We need it to demonstrate us research and experiment with live code, so we are able to easily and transparently trace what is happening to the request and what packages and functions are responsible for hidden validation rules (i.e. other, additional ones that do not appear explicitly in the OpenAPI schema).

Let's go:

`go install github.com/go-delve/delve/cmd/dlv@latest`

## DWARF
Package dwarf provides access to DWARF debugging information loaded from executable files, as defined in the DWARF 2.0 Standard at http://dwarfstd.org/doc/dwarf-2.0.0.pdf.

In our case, using the above knowledge is necessary to build a kube-apiserver (which serves as a gateway, an entry point to kubernetes, and which is responsible for all the validation that happens to the resource on entry, using its own logic, from etcd and from other kubernetes modules).
Let's build a kuber-apiserver with the data necessary for the debugger to associate function calls in the built application with breakpoints in the code.

### kube-apiserver binnary setup with DWARF
Please run this command from the directory where your kubernetes project was downloaded.
`date && make kube-apiserver -d DBG=1 && date`

### Hint: Log verbosity descriptions
Note: If you like to see more debug info from kube-apiserver (may be useful, but it's not necessary in our research example)

Log verbosity descriptions:

+ --v=0 Always visible to an Operator.
+ --v=1 A reasonable default log level if you do not want verbosity.
+ --v=2 Useful steady state information about the service and important log messages that might correlate to significant changes in the system. This is the recommended default log level.
+ --v=3 Extended information about changes.
+ --v=4 Debug level verbosity.

Yes, there's no v=5.

+ --v=6 Display requested resources.
+ --v=7 Display HTTP request headers.
+ --v=8 Display HTTP request contents.

### Prepare kube-apiserver and dlv together
To run previously compiled `kube-apiserver` with the necessary DWARF metadata needed for dlv debugger to work properly with the codebase, let's run:

`./kube-apiserver --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt --v=4`

`dlv --listen=:2379 --headless=true --api-version=2 --accept-multiclient exec ./kube-apiserver`

```
dlv --listen=:2379 --headless=true --api-version=2 --accept-multiclient exec ./kube-apiserver2 --etcd-servers http://localhost:2379 \
--service-account-key-file=service-account-key.pem \
--service-account-signing-key-file=service-account-key.pem \
--service-account-issuer=api \
--tls-cert-file=server.crt \
--tls-private-key-file=server.key \
--client-ca-file=ca.crt
```

Hint: there's an example output that you should expect if you decided to run dlv from GoLand's wrapper (not directly from the terminal):
`/home/adrian/Documents/GoLand-2023.2/plugins/go-plugin/lib/dlv/linux/dlv --listen=127.0.0.1:36221 --headless=true --api-version=2 --check-go-version=false --only-same-user=false exec /home/adrian/.cache/JetBrains/GoLand2023.2/tmp/GoLand/___go_build_k8s_io_kubernetes_cmd_kube_apiserver -- --etcd-servers http://localhost:2379 --service-account-key-file=service-account-key.pem --service-account-signing-key-file=service-account-key.pem --service-account-issuer=api --tls-cert-file=server.crt --tls-private-key-file=server.key --client-ca-file=ca.crt`

## Example with ingress:

Note: one field is not mandatory in schema (add png), it might be spotted only in the code.

`kubectl apply -f minimal-ingress-test-example-only-name-is-required-port-is-not-required-ingress-service-backend.yaml`
`kubectl delete -f minimal-ingress-test-example-only-name-is-required-port-is-not-required-ingress-service-backend.yaml`

Or with our configuration from 2nd step:

`kubectl apply -f minimal-ingress-test-example-only-name-is-required-port-is-not-required-ingress-service-backend.yaml --kubeconfig kubeconfig`
`kubectl delete -f minimal-ingress-test-example-only-name-is-required-port-is-not-required-ingress-service-backend.yaml --kubeconfig kubeconfig`

Hint: You can also connect to a remote computer (a host) and attach the debugger to the Go process that runs on the host. The remote debugger (Delve) must be running on the remote computer.

In case of issues with `yama`/`ptrace_scope` like `Could not attach to pid 320441:` this could be caused by a kernel security setting, try writing `0` to `/proc/sys/kernel/yama/ptrace_scope`

`Yama is a Linux Security Module that collects system-wide DAC security protections that are not handled by the core kernel itself. This is selectable at build-time with CONFIG_SECURITY_YAMA, and can be controlled at run-time through sysctls in /proc/sys/kernel/yama:`

More: https://www.kernel.org/doc/html/v4.15/admin-guide/LSM/Yama.html

`gops` is a command to list and diagnose Go processes currently running on your system.

![Screenshot](img/goland_breakpoint_works_remote_debug.png)

# 15 References

* https://kubernetes.io/blog/2023/04/24/openapi-v3-field-validation-ga/

* https://kubernetes.io/docs/reference/using-api/cel/

* https://github.com/kubernetes/enhancements/issues/2885

* https://github.com/golang/vscode-go/wiki/debugging#remote-debugging

* https://github.com/pwndbg/pwndbg

* https://docs.openshift.com/container-platform/4.8/rest_api/editing-kubelet-log-level-verbosity.html

* https://kubernetes.io/docs/concepts/cluster-administration/logging/

* https://github.com/kubernetes/enhancements/issues/2885

* https://github.com/google/gops/



#### Example of Ingress.json
Example of `Ingress.json`: https://gitlab.cern.ch/jeedy-public/kubernetes-json-schemas/-/raw/master/v1.27.1-standalone/ingress.json?ref_type=heads

<details>
<summary>
Expand `Ingress.json`
</summary>

```
{
  "description": "Ingress is a collection of rules that allow inbound connections to reach the endpoints defined by a backend. An Ingress can be configured to give services externally-reachable urls, load balance traffic, terminate SSL, offer name based virtual hosting etc.",
  "properties": {
    "apiVersion": {
      "description": "APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources",
      "type": [
        "string",
        "null"
      ]
    },
    "kind": {
      "description": "Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds",
      "type": [
        "string",
        "null"
      ],
      "enum": [
        "Ingress"
      ]
    },
    "metadata": {
      "description": "ObjectMeta is metadata that all persisted resources must have, which includes all objects users must create.",
      "properties": {
        "annotations": {
          "additionalProperties": {
            "type": [
              "string",
              "null"
            ]
          },
          "description": "Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata. They are not queryable and should be preserved when modifying objects. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations",
          "type": [
            "object",
            "null"
          ]
        },
        "creationTimestamp": {
          "description": "Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.",
          "format": "date-time",
          "type": [
            "string",
            "null"
          ]
        },
        "deletionGracePeriodSeconds": {
          "description": "Number of seconds allowed for this object to gracefully terminate before it will be removed from the system. Only set when deletionTimestamp is also set. May only be shortened. Read-only.",
          "format": "int64",
          "type": [
            "integer",
            "null"
          ]
        },
        "deletionTimestamp": {
          "description": "Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.",
          "format": "date-time",
          "type": [
            "string",
            "null"
          ]
        },
        "finalizers": {
          "description": "Must be empty before the object is deleted from the registry. Each entry is an identifier for the responsible component that will remove the entry from the list. If the deletionTimestamp of the object is non-nil, entries in this list can only be removed. Finalizers may be processed and removed in any order.  Order is NOT enforced because it introduces significant risk of stuck finalizers. finalizers is a shared field, any actor with permission can reorder it. If the finalizer list is processed in order, then this can lead to a situation in which the component responsible for the first finalizer in the list is waiting for a signal (field value, external system, or other) produced by a component responsible for a finalizer later in the list, resulting in a deadlock. Without enforced ordering finalizers are free to order amongst themselves and are not vulnerable to ordering changes in the list.",
          "items": {
            "type": [
              "string",
              "null"
            ]
          },
          "type": [
            "array",
            "null"
          ],
          "x-kubernetes-patch-strategy": "merge"
        },
        "generateName": {
          "description": "GenerateName is an optional prefix, used by the server, to generate a unique name ONLY IF the Name field has not been provided. If this field is used, the name returned to the client will be different than the name passed. This value will also be combined with a unique suffix. The provided value has the same validation rules as the Name field, and may be truncated by the length of the suffix required to make the value unique on the server.\n\nIf this field is specified and the generated name exists, the server will return a 409.\n\nApplied only if Name is not specified. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#idempotency",
          "type": [
            "string",
            "null"
          ]
        },
        "generation": {
          "description": "A sequence number representing a specific generation of the desired state. Populated by the system. Read-only.",
          "format": "int64",
          "type": [
            "integer",
            "null"
          ]
        },
        "labels": {
          "additionalProperties": {
            "type": [
              "string",
              "null"
            ]
          },
          "description": "Map of string keys and values that can be used to organize and categorize (scope and select) objects. May match selectors of replication controllers and services. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels",
          "type": [
            "object",
            "null"
          ]
        },
        "managedFields": {
          "description": "ManagedFields maps workflow-id and version to the set of fields that are managed by that workflow. This is mostly for internal housekeeping, and users typically shouldn't need to set or understand this field. A workflow can be the user's name, a controller's name, or the name of a specific apply path like \"ci-cd\". The set of fields is always in the version that the workflow used when modifying the object.",
          "items": {
            "description": "ManagedFieldsEntry is a workflow-id, a FieldSet and the group version of the resource that the fieldset applies to.",
            "properties": {
              "apiVersion": {
                "description": "APIVersion defines the version of this resource that this field set applies to. The format is \"group/version\" just like the top-level APIVersion field. It is necessary to track the version of a field set because it cannot be automatically converted.",
                "type": [
                  "string",
                  "null"
                ]
              },
              "fieldsType": {
                "description": "FieldsType is the discriminator for the different fields format and version. There is currently only one possible value: \"FieldsV1\"",
                "type": [
                  "string",
                  "null"
                ]
              },
              "fieldsV1": {
                "description": "FieldsV1 stores a set of fields in a data structure like a Trie, in JSON format.\n\nEach key is either a '.' representing the field itself, and will always map to an empty set, or a string representing a sub-field or item. The string will follow one of these four formats: 'f:<name>', where <name> is the name of a field in a struct, or key in a map 'v:<value>', where <value> is the exact json formatted value of a list item 'i:<index>', where <index> is position of a item in a list 'k:<keys>', where <keys> is a map of  a list item's key fields to their unique values If a key maps to an empty Fields value, the field that key represents is part of the set.\n\nThe exact format is defined in sigs.k8s.io/structured-merge-diff",
                "type": [
                  "object",
                  "null"
                ]
              },
              "manager": {
                "description": "Manager is an identifier of the workflow managing these fields.",
                "type": [
                  "string",
                  "null"
                ]
              },
              "operation": {
                "description": "Operation is the type of operation which lead to this ManagedFieldsEntry being created. The only valid values for this field are 'Apply' and 'Update'.",
                "type": [
                  "string",
                  "null"
                ]
              },
              "subresource": {
                "description": "Subresource is the name of the subresource used to update that object, or empty string if the object was updated through the main resource. The value of this field is used to distinguish between managers, even if they share the same name. For example, a status update will be distinct from a regular update using the same manager name. Note that the APIVersion field is not related to the Subresource field and it always corresponds to the version of the main resource.",
                "type": [
                  "string",
                  "null"
                ]
              },
              "time": {
                "description": "Time is a wrapper around time.Time which supports correct marshaling to YAML and JSON.  Wrappers are provided for many of the factory methods that the time package offers.",
                "format": "date-time",
                "type": [
                  "string",
                  "null"
                ]
              }
            },
            "type": [
              "object",
              "null"
            ]
          },
          "type": [
            "array",
            "null"
          ]
        },
        "name": {
          "description": "Name must be unique within a namespace. Is required when creating resources, although some resources may allow a client to request the generation of an appropriate name automatically. Name is primarily intended for creation idempotence and configuration definition. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#names",
          "type": [
            "string",
            "null"
          ]
        },
        "namespace": {
          "description": "Namespace defines the space within which each name must be unique. An empty namespace is equivalent to the \"default\" namespace, but \"default\" is the canonical representation. Not all objects are required to be scoped to a namespace - the value of this field for those objects will be empty.\n\nMust be a DNS_LABEL. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces",
          "type": [
            "string",
            "null"
          ]
        },
        "ownerReferences": {
          "description": "List of objects depended by this object. If ALL objects in the list have been deleted, this object will be garbage collected. If this object is managed by a controller, then an entry in this list will point to this controller, with the controller field set to true. There cannot be more than one managing controller.",
          "items": {
            "description": "OwnerReference contains enough information to let you identify an owning object. An owning object must be in the same namespace as the dependent, or be cluster-scoped, so there is no namespace field.",
            "properties": {
              "apiVersion": {
                "description": "API version of the referent.",
                "type": "string"
              },
              "blockOwnerDeletion": {
                "description": "If true, AND if the owner has the \"foregroundDeletion\" finalizer, then the owner cannot be deleted from the key-value store until this reference is removed. See https://kubernetes.io/docs/concepts/architecture/garbage-collection/#foreground-deletion for how the garbage collector interacts with this field and enforces the foreground deletion. Defaults to false. To set this field, a user needs \"delete\" permission of the owner, otherwise 422 (Unprocessable Entity) will be returned.",
                "type": [
                  "boolean",
                  "null"
                ]
              },
              "controller": {
                "description": "If true, this reference points to the managing controller.",
                "type": [
                  "boolean",
                  "null"
                ]
              },
              "kind": {
                "description": "Kind of the referent. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds",
                "type": "string"
              },
              "name": {
                "description": "Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#names",
                "type": "string"
              },
              "uid": {
                "description": "UID of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#uids",
                "type": "string"
              }
            },
            "required": [
              "apiVersion",
              "kind",
              "name",
              "uid"
            ],
            "type": [
              "object",
              "null"
            ],
            "x-kubernetes-map-type": "atomic"
          },
          "type": [
            "array",
            "null"
          ],
          "x-kubernetes-patch-merge-key": "uid",
          "x-kubernetes-patch-strategy": "merge"
        },
        "resourceVersion": {
          "description": "An opaque value that represents the internal version of this object that can be used by clients to determine when objects have changed. May be used for optimistic concurrency, change detection, and the watch operation on a resource or set of resources. Clients must treat these values as opaque and passed unmodified back to the server. They may only be valid for a particular resource or set of resources.\n\nPopulated by the system. Read-only. Value must be treated as opaque by clients and . More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency",
          "type": [
            "string",
            "null"
          ]
        },
        "selfLink": {
          "description": "Deprecated: selfLink is a legacy read-only field that is no longer populated by the system.",
          "type": [
            "string",
            "null"
          ]
        },
        "uid": {
          "description": "UID is the unique in time and space value for this object. It is typically generated by the server on successful creation of a resource and is not allowed to change on PUT operations.\n\nPopulated by the system. Read-only. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#uids",
          "type": [
            "string",
            "null"
          ]
        }
      },
      "type": [
        "object",
        "null"
      ]
    },
    "spec": {
      "description": "IngressSpec describes the Ingress the user wishes to exist.",
      "properties": {
        "defaultBackend": {
          "description": "IngressBackend describes all endpoints for a given service and port.",
          "properties": {
            "resource": {
              "description": "TypedLocalObjectReference contains enough information to let you locate the typed referenced object inside the same namespace.",
              "properties": {
                "apiGroup": {
                  "description": "APIGroup is the group for the resource being referenced. If APIGroup is not specified, the specified Kind must be in the core API group. For any other third-party types, APIGroup is required.",
                  "type": [
                    "string",
                    "null"
                  ]
                },
                "kind": {
                  "description": "Kind is the type of resource being referenced",
                  "type": "string"
                },
                "name": {
                  "description": "Name is the name of resource being referenced",
                  "type": "string"
                }
              },
              "required": [
                "kind",
                "name"
              ],
              "type": [
                "object",
                "null"
              ],
              "x-kubernetes-map-type": "atomic"
            },
            "service": {
              "description": "IngressServiceBackend references a Kubernetes Service as a Backend.",
              "properties": {
                "name": {
                  "description": "name is the referenced service. The service must exist in the same namespace as the Ingress object.",
                  "type": "string"
                },
                "port": {
                  "description": "ServiceBackendPort is the service port being referenced.",
                  "properties": {
                    "name": {
                      "description": "name is the name of the port on the Service. This is a mutually exclusive setting with \"Number\".",
                      "type": [
                        "string",
                        "null"
                      ]
                    },
                    "number": {
                      "description": "number is the numerical port number (e.g. 80) on the Service. This is a mutually exclusive setting with \"Name\".",
                      "format": "int32",
                      "type": [
                        "integer",
                        "null"
                      ]
                    }
                  },
                  "type": [
                    "object",
                    "null"
                  ]
                }
              },
              "required": [
                "name"
              ],
              "type": [
                "object",
                "null"
              ]
            }
          },
          "type": [
            "object",
            "null"
          ]
        },
        "ingressClassName": {
          "description": "ingressClassName is the name of an IngressClass cluster resource. Ingress controller implementations use this field to know whether they should be serving this Ingress resource, by a transitive connection (controller -> IngressClass -> Ingress resource). Although the `kubernetes.io/ingress.class` annotation (simple constant name) was never formally defined, it was widely supported by Ingress controllers to create a direct binding between Ingress controller and Ingress resources. Newly created Ingress resources should prefer using the field. However, even though the annotation is officially deprecated, for backwards compatibility reasons, ingress controllers should still honor that annotation if present.",
          "type": [
            "string",
            "null"
          ]
        },
        "rules": {
          "description": "rules is a list of host rules used to configure the Ingress. If unspecified, or no rule matches, all traffic is sent to the default backend.",
          "items": {
            "description": "IngressRule represents the rules mapping the paths under a specified host to the related backend services. Incoming requests are first evaluated for a host match, then routed to the backend associated with the matching IngressRuleValue.",
            "properties": {
              "host": {
                "description": "host is the fully qualified domain name of a network host, as defined by RFC 3986. Note the following deviations from the \"host\" part of the URI as defined in RFC 3986: 1. IPs are not allowed. Currently an IngressRuleValue can only apply to\n   the IP in the Spec of the parent Ingress.\n2. The `:` delimiter is not respected because ports are not allowed.\n\t  Currently the port of an Ingress is implicitly :80 for http and\n\t  :443 for https.\nBoth these may change in the future. Incoming requests are matched against the host before the IngressRuleValue. If the host is unspecified, the Ingress routes all traffic based on the specified IngressRuleValue.\n\nhost can be \"precise\" which is a domain name without the terminating dot of a network host (e.g. \"foo.bar.com\") or \"wildcard\", which is a domain name prefixed with a single wildcard label (e.g. \"*.foo.com\"). The wildcard character '*' must appear by itself as the first DNS label and matches only a single label. You cannot have a wildcard label by itself (e.g. Host == \"*\"). Requests will be matched against the Host field in the following way: 1. If host is precise, the request matches this rule if the http host header is equal to Host. 2. If host is a wildcard, then the request matches this rule if the http host header is to equal to the suffix (removing the first label) of the wildcard rule.",
                "type": [
                  "string",
                  "null"
                ]
              },
              "http": {
                "description": "HTTPIngressRuleValue is a list of http selectors pointing to backends. In the example: http://<host>/<path>?<searchpart> -> backend where where parts of the url correspond to RFC 3986, this resource will be used to match against everything after the last '/' and before the first '?' or '#'.",
                "properties": {
                  "paths": {
                    "description": "paths is a collection of paths that map requests to backends.",
                    "items": {
                      "description": "HTTPIngressPath associates a path with a backend. Incoming urls matching the path are forwarded to the backend.",
                      "properties": {
                        "backend": {
                          "description": "IngressBackend describes all endpoints for a given service and port.",
                          "properties": {
                            "resource": {
                              "description": "TypedLocalObjectReference contains enough information to let you locate the typed referenced object inside the same namespace.",
                              "properties": {
                                "apiGroup": {
                                  "description": "APIGroup is the group for the resource being referenced. If APIGroup is not specified, the specified Kind must be in the core API group. For any other third-party types, APIGroup is required.",
                                  "type": [
                                    "string",
                                    "null"
                                  ]
                                },
                                "kind": {
                                  "description": "Kind is the type of resource being referenced",
                                  "type": "string"
                                },
                                "name": {
                                  "description": "Name is the name of resource being referenced",
                                  "type": "string"
                                }
                              },
                              "required": [
                                "kind",
                                "name"
                              ],
                              "type": [
                                "object",
                                "null"
                              ],
                              "x-kubernetes-map-type": "atomic"
                            },
                            "service": {
                              "description": "IngressServiceBackend references a Kubernetes Service as a Backend.",
                              "properties": {
                                "name": {
                                  "description": "name is the referenced service. The service must exist in the same namespace as the Ingress object.",
                                  "type": "string"
                                },
                                "port": {
                                  "description": "ServiceBackendPort is the service port being referenced.",
                                  "properties": {
                                    "name": {
                                      "description": "name is the name of the port on the Service. This is a mutually exclusive setting with \"Number\".",
                                      "type": [
                                        "string",
                                        "null"
                                      ]
                                    },
                                    "number": {
                                      "description": "number is the numerical port number (e.g. 80) on the Service. This is a mutually exclusive setting with \"Name\".",
                                      "format": "int32",
                                      "type": [
                                        "integer",
                                        "null"
                                      ]
                                    }
                                  },
                                  "type": [
                                    "object",
                                    "null"
                                  ]
                                }
                              },
                              "required": [
                                "name"
                              ],
                              "type": [
                                "object",
                                "null"
                              ]
                            }
                          },
                          "type": "object"
                        },
                        "path": {
                          "description": "path is matched against the path of an incoming request. Currently it can contain characters disallowed from the conventional \"path\" part of a URL as defined by RFC 3986. Paths must begin with a '/' and must be present when using PathType with value \"Exact\" or \"Prefix\".",
                          "type": [
                            "string",
                            "null"
                          ]
                        },
                        "pathType": {
                          "description": "pathType determines the interpretation of the path matching. PathType can be one of the following values: * Exact: Matches the URL path exactly. * Prefix: Matches based on a URL path prefix split by '/'. Matching is\n  done on a path element by element basis. A path element refers is the\n  list of labels in the path split by the '/' separator. A request is a\n  match for path p if every p is an element-wise prefix of p of the\n  request path. Note that if the last element of the path is a substring\n  of the last element in request path, it is not a match (e.g. /foo/bar\n  matches /foo/bar/baz, but does not match /foo/barbaz).\n* ImplementationSpecific: Interpretation of the Path matching is up to\n  the IngressClass. Implementations can treat this as a separate PathType\n  or treat it identically to Prefix or Exact path types.\nImplementations are required to support all path types.",
                          "type": "string"
                        }
                      },
                      "required": [
                        "pathType",
                        "backend"
                      ],
                      "type": [
                        "object",
                        "null"
                      ]
                    },
                    "type": "array",
                    "x-kubernetes-list-type": "atomic"
                  }
                },
                "required": [
                  "paths"
                ],
                "type": [
                  "object",
                  "null"
                ]
              }
            },
            "type": [
              "object",
              "null"
            ]
          },
          "type": [
            "array",
            "null"
          ],
          "x-kubernetes-list-type": "atomic"
        },
        "tls": {
          "description": "tls represents the TLS configuration. Currently the Ingress only supports a single TLS port, 443. If multiple members of this list specify different hosts, they will be multiplexed on the same port according to the hostname specified through the SNI TLS extension, if the ingress controller fulfilling the ingress supports SNI.",
          "items": {
            "description": "IngressTLS describes the transport layer security associated with an ingress.",
            "properties": {
              "hosts": {
                "description": "hosts is a list of hosts included in the TLS certificate. The values in this list must match the name/s used in the tlsSecret. Defaults to the wildcard host setting for the loadbalancer controller fulfilling this Ingress, if left unspecified.",
                "items": {
                  "type": [
                    "string",
                    "null"
                  ]
                },
                "type": [
                  "array",
                  "null"
                ],
                "x-kubernetes-list-type": "atomic"
              },
              "secretName": {
                "description": "secretName is the name of the secret used to terminate TLS traffic on port 443. Field is left optional to allow TLS routing based on SNI hostname alone. If the SNI host in a listener conflicts with the \"Host\" header field used by an IngressRule, the SNI host is used for termination and value of the \"Host\" header is used for routing.",
                "type": [
                  "string",
                  "null"
                ]
              }
            },
            "type": [
              "object",
              "null"
            ]
          },
          "type": [
            "array",
            "null"
          ],
          "x-kubernetes-list-type": "atomic"
        }
      },
      "type": [
        "object",
        "null"
      ]
    },
    "status": {
      "description": "IngressStatus describe the current state of the Ingress.",
      "properties": {
        "loadBalancer": {
          "description": "IngressLoadBalancerStatus represents the status of a load-balancer.",
          "properties": {
            "ingress": {
              "description": "ingress is a list containing ingress points for the load-balancer.",
              "items": {
                "description": "IngressLoadBalancerIngress represents the status of a load-balancer ingress point.",
                "properties": {
                  "hostname": {
                    "description": "hostname is set for load-balancer ingress points that are DNS based.",
                    "type": [
                      "string",
                      "null"
                    ]
                  },
                  "ip": {
                    "description": "ip is set for load-balancer ingress points that are IP based.",
                    "type": [
                      "string",
                      "null"
                    ]
                  },
                  "ports": {
                    "description": "ports provides information about the ports exposed by this LoadBalancer.",
                    "items": {
                      "description": "IngressPortStatus represents the error condition of a service port",
                      "properties": {
                        "error": {
                          "description": "error is to record the problem with the service port The format of the error shall comply with the following rules: - built-in error values shall be specified in this file and those shall use\n  CamelCase names\n- cloud provider specific error values must have names that comply with the\n  format foo.example.com/CamelCase.",
                          "type": [
                            "string",
                            "null"
                          ]
                        },
                        "port": {
                          "description": "port is the port number of the ingress port.",
                          "format": "int32",
                          "type": "integer"
                        },
                        "protocol": {
                          "description": "protocol is the protocol of the ingress port. The supported values are: \"TCP\", \"UDP\", \"SCTP\"",
                          "type": "string"
                        }
                      },
                      "required": [
                        "port",
                        "protocol"
                      ],
                      "type": [
                        "object",
                        "null"
                      ]
                    },
                    "type": [
                      "array",
                      "null"
                    ],
                    "x-kubernetes-list-type": "atomic"
                  }
                },
                "type": [
                  "object",
                  "null"
                ]
              },
              "type": [
                "array",
                "null"
              ]
            }
          },
          "type": [
            "object",
            "null"
          ]
        }
      },
      "type": [
        "object",
        "null"
      ]
    }
  },
  "type": "object",
  "x-kubernetes-group-version-kind": [
    {
      "group": "networking.k8s.io",
      "kind": "Ingress",
      "version": "v1"
    }
  ],
  "$schema": "http://json-schema.org/schema#"
}
```
</details>
